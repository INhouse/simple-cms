<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Section;
use App\Exceptions\ContentNotFoundException;
use App\Exceptions\SubsectionNotFoundException;

/**
 * Test the methods to access structure elements in Section
 */
class SectionMethodsTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Test if the right exception is thrown when the content is
     * not present in the section.
     */
    public function testContentNotFoundException()
    {
        $this->expectException(ContentNotFoundException::class);
        $section = factory(Section::class)->create();
        $section->getContent('key');
    }

    /**
     * Test if the right exception is thrown when a stock of content
     * elements are not present in the section.
     */
    public function testStockContentNotFoundException()
    {
        $this->expectException(ContentNotFoundException::class);
        $section = factory(Section::class)->create();
        $section->getStock('key');
    }

    /**
     * Test if the right exception is thrown when a subsection doesn't
     * exists in the section.
     */
    public function testSubsectionNotFoundException()
    {
        $this->expectException(SubsectionNotFoundException::class);
        $section = factory(Section::class)->create();
        $section->getSubsection('key');
    }

    /**
     * Test if the method getSubsection return a collection of Section
     * models with the right types.
     */
    public function testSubsections()
    {
        $section = factory(Section::class)->create();
        $content = factory(Section::class)->state('content')->make();
        $section->child_sections()->save($content);

        $amount = 7;
        $subsections = factory(Section::class, $amount)->make();
        $section->child_sections()->saveMany($subsections);

        $subsections_ = $section->getSubsections();

        $this->assertNotNull($subsections_);
        $this->assertEquals($amount, $subsections_->count());
    }

    /**
     * Test if a specific subsection can be obtained by its key.
     */
    public function testSubsection()
    {
        $section = factory(Section::class)->create();
        $subsection = factory(Section::class)->make();

        $section->child_sections()->save($subsection);

        $subsection_ = $section->getSubsection($subsection->key);

        $this->assertNotNull($subsection_);
        $this->assertEquals($subsection->name, $subsection_->name);
    }

    /**
     * Test if a Content element can be obtained by its structure key.
     */
    public function testContent()
    {
        $section = factory(Section::class)->create();
        $content_section = factory(Section::class)->state('content')->create();
        $section->child_sections()->save($content_section);

        $content = $section->getContent($content_section->key);

        $this->assertNotNull($content);
        $this->assertEquals($content_section->name, $content->section->name);
    }

    /**
     * Test if a collection of Content elements can be obtained by its
     * structure key.
     */
    public function testStock()
    {
        $section = factory(Section::class)->create();
        $stock_section = factory(Section::class)->state('stock')->create();
        $section->child_sections()->save($stock_section);

        $stock = $section->getStock($stock_section->key);

        $this->assertNotNull($stock);
        $this->assertGreaterThan(1, $stock->count());
    }

    /**
     * Test if a collection can be filtered with a closure
     */
    public function testStockFilter()
    {
        $section = factory(Section::class)->create();
        $stock_section = factory(Section::class)->state('stock')->create();
        $section->child_sections()->save($stock_section);

        $stock = $section->getStock($stock_section->key, function($contents) {
            return $contents->take(1)->get();
        });

        $this->assertNotNull($stock);
        $this->assertEquals(1, $stock->count());
    }
}
