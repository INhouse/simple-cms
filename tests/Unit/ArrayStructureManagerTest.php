<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Section;
use App\Services\ArrayStructureManager;

use App\Exceptions\NewSectionNotAllowedException;
use App\Exceptions\NoContentException;
use App\Exceptions\WrongSectionTypeException;

class ArrayStructureManagerTest extends TestCase
{
    use RefreshDatabase;

    const BASIC_STRUCTURE = [
        'home' => [
            ':name' => 'Home',
            ':type' => 'page',
            ':content' => [
                'welcome' => [
                    'title' => 'text',
                    'message' => 'textarea',
                ],
                'news' => [
                    ':stock' => true,
                    ':fields' => [
                        'title' => 'text',
                        'body' => 'textarea',
                    ],
                ],
            ],
        ],
    ];

    const FULL_STRUCTURE = [
        'home' => [
            ':name' => 'Home',
            ':type' => 'page',
            ':content' => [
                'banner' => [
                    ':name' => 'Banner',
                    ':stock' => true,
                    ':fields' => [
                        'title' => 'text',
                        'image' => 'image=700:400',
                    ],
                ],
                'information' => [
                    'title' => 'text',
                    'body' => 'textarea',
                ],
            ],
        ],
        'products' => [
            ':name' => 'Products',
            ':type' => 'category',
            ':content' => [
                'description' => [
                    'title' => 'text',
                    'description' => 'textarea',
                ],
            ],
            ':template' => [
                'name' => 'text',
                'description' => 'textarea',
                'image' => 'image=50:50',
                'price' => 'price',
            ],
            ':subsections' => [
                'category-a' => [
                    ':name' => 'Category A',
                    ':type' => 'page',
                    ':content' => [
                        'products' => [
                            ':name' => 'Products',
                            ':stock' => true,
                            ':fields' => ':template',
                        ],
                    ],
                ],
                ':new' => [
                    ':name' => '',
                    ':type' => 'category',
                    ':subsections' => [
                        ':new' => [
                            ':name' => '',
                            ':type' => 'page',
                            ':content' => [
                                'description' => [
                                    'title' => 'text',
                                    'description' => 'textarea',
                                    'cover' => 'image',
                                ],
                                'products' => [
                                    ':name' => 'Products',
                                    ':stock' => true,
                                    ':fields' => ':template',
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ];

    /**
     * Property to store ArrayStructureManager instance
     */
    private $manager;

    /**
     * Method to build the structure capturing the output
     */
    private function buildStructure($structure)
    {
        config(['structure.site' => $structure]);
        ob_start();
        $this->manager->build();
        $output = ob_get_clean();
    }

    /**
     * Test a basic structure build
     */
    public function testBuildStructure()
    {
        $this->manager = new ArrayStructureManager();
        $this->buildStructure(self::BASIC_STRUCTURE);

        $home = Section::where('slug', 'home')->first();
        $this->assertNotNull($home);

        $welcome = $home->getContent('welcome');
        $this->assertNotNull($welcome);

        $news = $home->getStock('news');
        $this->assertNotNull($news);
    }

    /**
     * Test a more complex structure build
     */
    public function testFullBuild()
    {
        $this->manager = new ArrayStructureManager();
        $this->buildStructure(self::FULL_STRUCTURE);

        $home = Section::where('slug', 'home')->first();
        $this->assertNotNull($home);

        $banner = $home->getStock('banner');
        $this->assertNotNull($banner);

        $information = $home->getContent('information');
        $this->assertNotNull($information);

        $products = Section::where('slug', 'products')->first();
        $this->assertNotNull($products);

        $description = $products->getContent('description');
        $this->assertNotNull($description);

        $subsections = $products->getSubsections();
        $this->assertGreaterThan(0, $subsections->count());

        $category_a = $products->getSubsection('category-a');
        $this->assertNotNull($category_a);
    }

    /**
     * Test what happens when a multiple builds are run
     */
    public function testBuildIdempotency()
    {
        $this->manager = new ArrayStructureManager();
        $this->buildStructure(self::BASIC_STRUCTURE);
        $this->buildStructure(self::BASIC_STRUCTURE);

        $sections = Section::where('slug', 'home')->get();
        $this->assertEquals(1, $sections->count());

        $home = $sections->first();
        $this->assertEquals(2, $home->child_sections->count());

        $welcome_section = $home->child_sections()
            ->where('key', 'welcome')->first();
        $this->assertEquals(1, $welcome_section->contents->count());
    }

    /**
     * Test exception when Section is not of category type
     */
    public function testNewSubsectionWrongType()
    {
        $this->expectException(WrongSectionTypeException::class);

        $section = factory(Section::class)->create([
            'type' => '',
        ]);

        $this->manager = new ArrayStructureManager();
        $this->manager->getNewSubsection($section);
    }

    /**
     * Test exception when new element is not defined
     */
    public function testNewSubsectionNoNewAllowed()
    {
        $this->expectException(NewSectionNotAllowedException::class);

        $structure = [
            'home' => [
                ':name' => 'home',
                ':type' => 'category',
                ':subsections' => [
                    'other' => [
                        ':name' => 'Other',
                        ':type' => 'page',
                    ],
                ],
            ],
        ];

        $this->manager = new ArrayStructureManager();
        $this->buildStructure($structure);

        $home = Section::where('slug', 'home')->first();
        $this->manager->getNewSubsection($home);
    }

    /**
     * Test configuration for new subsection
     */
    public function testNewSubsection()
    {
        $this->manager = new ArrayStructureManager();
        $this->buildStructure(self::FULL_STRUCTURE);

        $section = Section::where('slug', 'products')->first();
        $new = $this->manager->getNewSubsection($section);
        $this->assertNotNull($new);
        $this->assertEquals(
            $new,
            self::FULL_STRUCTURE['products'][':subsections'][':new']
        );
    }

    /**
     * Test exception when content is requested on a section that is
     * not page or category.
     */
    public function testSectionContentWrongType()
    {
        $this->expectException(WrongSectionTypeException::class);

        $section = factory(Section::class)->create([
            'type' => '',
        ]);

        $this->manager = new ArrayStructureManager();
        $this->manager->getSectionContent($section);
    }

    /**
     * Test exception when content has not been defined
     */
    public function testSectionContentNoContent()
    {
        $this->expectException(NoContentException::class);

        $structure = [
            'home' => [
                ':name' => 'home',
                ':type' => 'page',
            ],
        ];

        $this->manager = new ArrayStructureManager();
        $this->buildStructure($structure);

        $section = Section::where('slug', 'home')->first();
        $this->manager->getSectionContent($section);
    }

    /**
     * Test getting the configuration for a section content
     */
    public function testSectionContent()
    {
        $this->manager = new ArrayStructureManager();
        $this->buildStructure(self::FULL_STRUCTURE);

        $home = Section::where('slug', 'home')->first();
        $content = $this->manager->getSectionContent($home);
        $this->assertNotNull($content);
        $this->assertEquals(
            $content,
            self::FULL_STRUCTURE['home'][':content']
        );
    }

    /**
     * Test exception when fields are reuquested on a section that is
     * not of type content or stock.
     */
    public function testFieldsWrongType()
    {
        $this->expectException(WrongSectionTypeException::class);

        $section = factory(Section::class)->create([
            'type' => '',
        ]);

        $this->manager = new ArrayStructureManager();
        $this->manager->getFields($section);
    }

    /**
     * Test exception when fields are requested but no content
     * is defined.
     */
    public function testFieldsNoContentArray()
    {
        $this->expectException(NoContentException::class);

        $structure = [
            'home' => [
                ':name' => 'home',
                ':type' => 'page',
            ],
        ];

        $this->manager = new ArrayStructureManager();
        $this->buildStructure($structure);

        $section = Section::where('slug', 'home')->first();
        $content = factory(Section::class)->state('content')->make();
        $section->child_sections()->save($content);

        $this->manager->getFields($content);
    }

    /**
     * Test exception when fields are requested on a section that
     * is not defined in content.
     */
    public function testFieldsNoContentKey()
    {
        $this->expectException(NoContentException::class);

        $structure = [
            'home' => [
                ':name' => 'home',
                ':type' => 'page',
                ':content' => [
                    'other' => [
                        'title' => 'text',
                    ],
                ],
            ],
        ];

        $this->manager = new ArrayStructureManager();
        $this->buildStructure($structure);

        $section = Section::where('slug', 'home')->first();
        $content = factory(Section::class)->state('content')->make();
        $section->child_sections()->save($content);

        $this->manager->getFields($content);
    }

    /**
     * Test getting the fields on a internal content section
     */
    public function testSectionFields()
    {
        $this->manager = new ArrayStructureManager();
        $this->buildStructure(self::FULL_STRUCTURE);

        $home = Section::where('slug', 'home')->first();
        $information = $home
        ->child_sections()
        ->where('key', 'information')->first();
        $fields = $this->manager->getFields($information);
        $this->assertNotNull($fields);
        $this->assertGreaterThan(0, count($fields));

        $fieldsconfig = self::FULL_STRUCTURE['home'][':content']['information'];
        foreach($fields as $field) {
            $type = $field->type . ($field->details?'='.$field->details:'');
            $this->assertEquals($type, $fieldsconfig[$field->name]);
        }
    }

    /**
     * Test getting the fields on content that is defined by a template
     */
    public function testFieldsTemplate()
    {
        $this->manager = new ArrayStructureManager();
        $this->buildStructure(self::FULL_STRUCTURE);

        $products = Section::where('slug', 'products')->first();
        $category = $products->getSubsection('category-a');
        $content = $category
        ->child_sections()
        ->where('key', 'products')->first();
        $fields = $this->manager->getFields($content);
        $this->assertNotNull($fields);
        $this->assertGreaterThan(0, count($fields));

        $fieldsconfig = self::FULL_STRUCTURE['products'][':template'];
        foreach($fields as $field) {
            $type = $field->type . ($field->details?'='.$field->details:'');
            $this->assertEquals($type, $fieldsconfig[$field->name]);
        }
    }
}
