<?php

namespace App\Services;

use App\Contracts\ImageEditor;


class ManualImageEditor implements ImageEditor
{
    public function initialize()
    {
        return '<script>console.log("image editor fake initialization");</script>';
    }

    public function createEditor($image, $aspect_ratio)
    {
        $html = '<img src="'.asset($image->url).'" alt="'.$image->name.'" /><br/>';
        $html .= '<input type="text" name="image_editor['.$image->label.'][width]" value="'.$image->width.'"/>';
        $html .= '<input type="text" name="image_editor['.$image->label.'][height]" value="'.$image->height.'"/>';
        $html .= '<input type="text" name="image_editor['.$image->label.'][x]" value="'.$image->x.'"/>';
        $html .= '<input type="text" name="image_editor['.$image->label.'][y]" value="'.$image->y.'"/>';

        return $html;
    }
}
