<?php

namespace App\Services;

use App\Section;
use App\Contracts\StructureManager;

use App\Exceptions\NewSectionNotAllowedException;
use App\Exceptions\NoContentException;
use App\Exceptions\WrongSectionTypeException;

/**
 * Structure Manager based on an array that contains the site structure
 */
class ArrayStructureManager implements StructureManager
{
    /**
     * @see StructureManager::build()
     */
    public function build()
    {
        $structure = config('structure.site');

        $this->insertSections($structure);
    }

    /**
     * @see StructureManager::getNewSubsection()
     */
    public function getNewSubsection($section) {
        if($section->type != 'category') {
            throw new WrongSectionTypeException(
                'Section is not a category'
            );
        }

        $config = $this->getSectionConfig($section);

        if(!key_exists(':new', $config[':subsections'])) {
            throw new NewSectionNotAllowedException(
                'No configuration for new sections'
            );
        }

        return $config[':subsections'][':new'];
    }

    /**
     * @see StructureManager::getSectionContent()
     */
    public function getSectionContent($section) {
        if($section->type != 'page' && $section->type != 'category') {
            throw new WrongSectionTypeException(
                'This section is not a page or category'
            );
        }

        $config = $this->getSectionConfig($section);
        if(!key_exists(':content', $config)) {
            throw new NoContentException(
                'No content is defined'
            );
        }

        $content = $config[':content'];
        return $content;
    }

    /**
     * @see StructureManager::getFields()
     */
    public function getFields($section)
    {
        if($section->type != 'content' && $section->type != 'stock') {
            throw new WrongSectionTypeException(
                'Section is not from type content or stock'
            );
        }

        $section_config = $this->getSectionConfig($section->parent_section);
        if(!key_exists(':content', $section_config)) {
            throw new NoContentException(
                'Parent section has not content defined'
            );
        }

        $content = $section_config[':content'];
        if(!key_exists($section->key, $content)) {
            throw new NoContentException(
                'No content defined for '.$section->key
            );
        }

        $config = $content[$section->key];

        if(key_exists(':fields', $config)) {
            $config = $config[':fields'];
        }

        if(is_string($config) && $config == ':template') {
            if(!key_exists(':template', $section_config)) {
                throw new NoContentException(
                    'Missing template for content'
                );
            }
            $config = $section_config[':template'];
        }

        $fields = [];
        foreach($config as $name => $type) {
            $details = '';
            if(strpos($type, '=') !== false) {
                list($type, $details) = explode('=', $type);
            }
            $fields[$name] = $this->createField($name, $type, $details);
        }
        return $fields;
    }

    /**
     * Insert to the database the provided list of sections
     *
     * Insert the list of sections to the database. This function is
     * recursive and calls itself when a list of subsections is
     * encountered.
     *
     * @param array $sections An array with the sections definitions
     * @param \App\Section $parent An optional parent section
     */
    private function insertSections($sections, $parent=NULL)
    {
        foreach($sections as $key => $data) {
            if($key == ':new') continue;

            if($parent) {
                $slug = $parent->slug.'-'.$key;
            }
            else {
                $slug = $key;
            }

            echo 'Creating section '.$data[':name'].PHP_EOL;
            $section = Section::firstOrNew(['slug' => $slug]);
            $section->name = $data[':name'];
            $section->key = $key;
            $section->type = $data[':type'];
            if($parent) {
                $parent->child_sections()->save($section);
            }
            else {
                $section->save();
            }

            if(key_exists(':content', $data)) {
                foreach($data[':content'] as $key => $content) {
                    $content_slug = $section->slug.'-'.$key;
                    $content_section = Section::firstOrNew(['slug' => $content_slug]);
                    if(key_exists(':name', $content)) {
                        $content_section->name = $content[':name'];
                    }
                    else {
                        $content_section->name = ucfirst($key);
                    }
                    $content_section->key = $key;
                    $content_section->type = 'content';
                    if(key_exists(':stock', $content) && $content[':stock']) {
                        $content_section->type = 'stock';
                    }
                    echo "Adding content $content_section->name to $section->name".PHP_EOL;
                    $section->child_sections()->save($content_section);

                    if($content_section->type == 'content'
                       && $content_section->contents->count() < 1) {
                        $content = $content_section->contents()->make();
                        $content->name = 'Content';
                        $content->slug = $content_section->slug.'-content';
                        $content->save();
                    }
                }
            }

            if($data[':type'] == 'category') {
                $subsections = $data[':subsections'];
                if(!empty($subsections) && !(count($subsections) == 1 && key_exists(':new', $subsections))) {
                    echo 'Creating subsections...'.PHP_EOL;
                    $this->insertSections($subsections, $section);
                    echo 'Finish subsections'.PHP_EOL;
                }
            }
        }
    }

    /**
     * Return the configuration block for a given section
     *
     * Search in the configuration array for the configuration element
     * that corresponds to the Section instance provided.
     *
     * @param \App\Section $section
     * @return array The configuration block for the section
     */
    private function getSectionConfig($section)
    {
        $current_section = $section;
        $trace = [];
        while($current_section->parent_section) {
            $trace[] = $current_section;
            $current_section = $current_section->parent_section;
        }
        $trace[] = $current_section;

        $config = config('structure.site');
        $slug_chain = '';
        $templated_fields = NULL;
        $fields_config = NULL;
        foreach(array_reverse($trace) as $trace_section) {
            $slug_chain = $trace_section->slug;
            $key = $trace_section->key;

            if(key_exists($key, $config)) {
                $section_config = $config[$key];
            }
            elseif (key_exists(':new', $config)) {
                $section_config = $config[':new'];
            }
            else {
                break;
            }

            if(key_exists(':template', $section_config)) {
                $templated_fields = $section_config[':template'];
            }

            if(key_exists(':subsections', $section_config)) {
                $config = $section_config[':subsections'];
            }
            else {
                break;
            }
        }

        $section_config[':template'] = $templated_fields;
        return $section_config;
    }

    /**
     * Create a standard class instance with field information
     *
     * This method provides compatibility with the current Controller
     * that creates the fields in the CMS. Is to be replaced in the
     * future.
     *
     * @param string $name Field name
     * @param string $type Field type
     * @param string $details Additional field configuration
     *
     * @return \stdClass An object with the field information
     */
    private function createField($name, $type, $details)
    {
        $field = new \stdClass();
        $field->name = $name;
        $field->form_name = str_slug($name);
        $field->type = $type;
        $field->value = '';
        $field->details = $details;
        return $field;
    }
}
