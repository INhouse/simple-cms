<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use App\Image as ImageModel;
use App\Document;
use App\Gallery;
use App\Contracts\MediaManager as MediaManagerContract;

class MediaManager implements MediaManagerContract
{
    public function storeImages($image_collection, $image_fields, $request) {
        if($image_fields) {
            foreach($image_fields as $image_name) {
                $filename = 'image_'.$image_name;
                if($request->hasFile($filename)) {
                    $image_file = $request->file($filename);
                    if($image_file->isValid()) {
                        $image = Image::make($image_file);
                        $model = new ImageModel();
                        $model->name = $image_file->getClientOriginalName();
                        $model->label = $image_name;
                        $model->file = $image_file->store('images', 'public');
                        // Get the width and height of the image
                        $model->width = $image->width();
                        $model->height = $image->height();
                        // Make sure that there are not two images
                        // with the same label
                        $this->deleteImage($image_collection, $image_name);
                        $image_collection->save($model);
                    }
                }
                else {
                    // Need a different query builder to avoid problems with where
                    $collection = clone $image_collection;
                    $model = $collection->where('label', $image_name)->first();

                    if($model){
                        $model->width = (int) $request->input('image_editor.'.$image_name.'.width');
                        $model->height = (int) $request->input('image_editor.'.$image_name.'.height');
                        $model->x = (int) $request->input('image_editor.'.$image_name.'.x');
                        $model->y = (int) $request->input('image_editor.'.$image_name.'.y');

                        if($model->isDirty()) {
                            $image = Image::make(Storage::disk('public')->get($model->file));
                            // Add a validation to only crop if dimentions are greater than 0
                            if($model->width > 0 && $model->height > 0) {
                                $image->crop($model->width, $model->height, $model->x, $model->y);
                            }
                            $version = uniqid();
                            $cropped_path = str_replace('images/', 'images/'.$version.'_', $model->file);
                            Storage::disk('public')->put($cropped_path, $image->stream('jpg'));
                            if(Storage::disk('public')->exists($model->cropped)) {
                                Storage::disk('public')->delete($model->cropped);
                            }
                            $model->cropped = $cropped_path;

                            $model->save();
                        }
                    }
                }
            }
        }
    }

    public function storeDocuments($document_collection, $document_fields, $request) {
        if($document_fields) {
            foreach($document_fields as $document_name) {
                $filename = 'document_'.$document_name;
                if($request->hasFile($filename)) {
                    $document_file = $request->file($filename);
                    if($document_file->isValid()) {
                        $document = new Document();
                        $document->name = $document_file->getClientOriginalName();
                        $document->extension = $document_file->getClientOriginalExtension();
                        $document->size = $document_file->getClientSize();
                        $document->label = $document_name;
                        $document->file = $document_file->store('documents', 'public');
                        // Nos aseguramos que no hayan dos documentos con la misma etiqueta
                        $this->deleteDocument($document_collection, $document_name);
                        $document_collection->save($document);
                    }
                }
            }
        }
    }

    public function storeGallery($name, $type, $elements, $request) {
        $gallery = new Gallery();
        $gallery->name = $name;
        $gallery->type = $type;
        // TODO (Javs) estos valores estas quemados por ahora
        $gallery->fixed_size = false;
        $gallery->width = 0;
        $gallery->height = 0;
        $gallery->save();

        if($elements) {
            if($type == 'images') {
                $this->storeImages($gallery->images(), $elements, $request);
            }
            elseif($type == 'documents') {
                $this->storeDocuments($gallery->documents(), $elements, $request);
            }
            else {
                // TODO (Javs) En el futuro disparar una excepción
                return NULL;
            }
        }

        return $gallery;
    }

    public function deleteImage($image_collection, $image_label) {
        $image = $image_collection->where('label', $image_label)->first();
        if($image) {
            $image->delete();
        }
    }

    public function deleteDocument($document_collection, $document_label) {
        $document = $document_collection->where('label', $document_label)->first();
        if($document) {
            $document->delete();
        }
    }
}
