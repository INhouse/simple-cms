<?php

namespace App\Services;

use App\Contracts\ImageEditor;


class CropperImageEditor implements ImageEditor
{
    public function initialize()
    {
        $scripts = '<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css" integrity="sha256-SyWqODkStAbrafgJ9K8twjULP7yL6yTRZKoFGjDYybI=" crossorigin="anonymous" />';
        $scripts .= '<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js" integrity="sha256-+PQmsBomnsT/06XEdk7ibt/9We4DX+VKzJsw/+DCzc0=" crossorigin="anonymous"></script>';
        $scripts .= '<script src="'.asset('js/services/cropper-image-editor.js').'"></script>';
        return $scripts;
    }

    public function createEditor($image, $aspect_ratio)
    {
        if ($aspect_ratio != '') {
            $array = explode(':', $aspect_ratio);
            // Verify that aspect ratio data is correct and is numeric
            if (count($array) == 2) {
                list($x, $y) = $array;
                if ((int) $y != 0) {
                    $aspect_ratio = ((int) $x)/((int) $y);
                }
            }
        }
        $html = '<div><img class="cropper-editor" data-aspect-ratio="'.$aspect_ratio.'" data-label="'.$image->label.'" src="'.asset($image->original).'" style="max-width: 100%" alt="'.$image->name.'"/></div>';
        $html .= '<input type="hidden" name="image_editor['.$image->label.'][width]" value="'.$image->width.'"/>';
        $html .= '<input type="hidden" name="image_editor['.$image->label.'][height]" value="'.$image->height.'"/>';
        $html .= '<input type="hidden" name="image_editor['.$image->label.'][x]" value="'.$image->x.'"/>';
        $html .= '<input type="hidden" name="image_editor['.$image->label.'][y]" value="'.$image->y.'"/>';
        return $html;
    }
}
