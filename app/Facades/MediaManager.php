<?php

namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class MediaManager extends Facade
{
    protected static function getFacadeAccessor() { return 'App\Contracts\MediaManager'; }
}
