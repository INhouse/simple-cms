<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Contracts\StructureManager;

class BuildSections extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'build:sections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Build the default website sections';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(StructureManager $structure)
    {
        echo 'building site structure...'.PHP_EOL;
        $structure->build();
        echo 'building finished!'.PHP_EOL;
    }
}
