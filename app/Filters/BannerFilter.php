<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;


class BannerFilter implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        // TODO (Javs) Obtener el tamaño del banner de un archivo de configuración
        return $image->resize(1280, null, function($constraint) {
            $constraint->aspectRatio();
        });
    }
}
