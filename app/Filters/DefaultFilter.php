<?php

namespace App\Filters;

use Intervention\Image\Image;
use Intervention\Image\Filters\FilterInterface;


class DefaultFilter implements FilterInterface
{
    public function applyFilter(Image $image)
    {
        $max_width = config('imagecache.default_max_width');
        if ($image->width() > $max_width) {
            return $image->resize($max_width, null, function($constraint) {
                $constraint->aspectRatio();
            });
        }

        return $image;
    }
}
