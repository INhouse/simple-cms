<?php

namespace App;

use App\Traits\HasMedia;
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    use HasMedia;

    protected $table = 'media_galleries';

    protected $casts = [
        'fixed_size' => 'boolean',
    ];

    public function contents() {
        return $this->belongsToMany('App\Content', 'cms_contents_media_galleries', 'media_galleries_id', 'cms_contents_id');
    }
}
