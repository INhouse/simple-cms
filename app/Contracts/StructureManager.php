<?php

namespace App\Contracts;

/**
 * Contract for a StructureManager that builds the site structure
 */
interface StructureManager
{
    /**
     * Build site structure in the database
     */
    public function build();

    /**
     * Return the configuration for a new subsection
     *
     * @param \App\Section $section
     * @return array
     * @throws \App\Exceptions\WrongSectionTypeException
     * @throws \App\Exceptions\NewSectionNotAllowedException
     */
    public function getNewSubsection($section);

    /**
     * Return the content configuration for a section
     *
     * @param \App\Section $section
     * @return array
     * @throws \App\Exceptions\WrongSectionTypeException
     * @throws \App\Exceptions\NoContentException
     */
    public function getSectionContent($section);

    /**
     * Returns the fields configuration for a section
     *
     * @param \App\Section $section
     * @return array
     * @throws \App\Exceptions\WrongSectionTypeException
     * @throws \App\Exceptions\NoContentException
     */
    public function getFields($section);
}
