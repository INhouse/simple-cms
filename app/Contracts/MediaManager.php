<?php

namespace App\Contracts;


interface MediaManager
{
    public function storeImages($image_collection, $image_fields, $request);

    public function storeDocuments($image_collection, $image_fields, $request);

    public function storeGallery($name, $type, $elements, $request);

    public function deleteImage($image_collection, $image_label);

    public function deleteDocument($document_collection, $document_label);
}
