<?php

namespace App\Contracts;


interface ImageEditor
{
    public function initialize();

    public function createEditor($image, $aspect_ratio);
}
