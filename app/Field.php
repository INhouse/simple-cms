<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    protected $table = 'cms_fields';

    protected $appends = ['form_name'];

    public function content() {
        return $this->belongsTo('App\Content');
    }

    public function getFormNameAttribute() {
        return str_slug($this->attributes['name']);
    }
}
