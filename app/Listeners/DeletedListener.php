<?php

namespace App\Listeners;

use App\Events\MediaDeleted;
use Illuminate\Support\Facades\Storage;

class DeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  MediaDeleted  $event
     * @return void
     */
    public function handle(MediaDeleted $event)
    {
        // TODO (Javs) Permitir que el sistema trabaje con otro tipo de discos
        Storage::disk('public')->delete($event->model->file);
    }
}
