<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use App\Exceptions\ContentNotFoundException;
use App\Exceptions\SubsectionNotFoundException;

/**
 * Contains the sections for the site structure
 *
 * @property int $id Section Primary key
 * @property int $section_id Foreign key to parent section
 * @property string $name Section name
 * @property string $slug Slug identifier
 * @property string $type Section type: page, category, content or stock
 * @property string $key Key for quick identification in structure
 *
 * @property \Illuminate\Database\Eloquent\Collection $child_sections
 * @property \App\Section $parent_section
 * @property \Illuminate\Database\Eloquent\Collection $contents
 *
 * @property \Carbon\Carbon $created_at Date of creation
 * @property \Carbon\Carbon $updated_at Date of last update
 */

class Section extends Model
{
    protected $table = 'cms_sections';

    protected $fillable = ['slug'];

    /**
     * Define the property that is used to identify the model on routes
     *
     * @return string
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Return the relationship for child sections
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function child_sections() {
        return $this->hasMany('App\Section');
    }

    /**
     * Return the relationship for the parent section
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent_section() {
        return $this->belongsTo('App\Section', 'section_id');
    }

    /**
     * Return the relationship for the section's content
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contents() {
        return $this->hasMany('App\Content');
    }

    /**
     * Return the content element in the structure with the given key
     *
     * This method is based in how the structure is handled in the
     * Structure Manager. It finds an internal section with the type
     * 'content' and returns its Content element.
     *
     * @param string $key The key that identifies the content
     * @return \App\Content
     * @throws \App\Exceptions\ContentNotFoundException
     */
    public function getContent($key)
    {
        $section = $this->child_sections()
            ->where('key', $key)
            ->where('type', 'content')
            ->first();
        if(!$section) {
            throw new ContentNotFoundException("Couldn't find $key content");
        }
        return $section->contents()->first();
    }

    /**
     * Return a collection of Content elements with the given key
     *
     * This method is based in how the structure is handled in the
     * Structure Manager. It finds an internal section with the type
     * 'stock' and returns all of its Content elements.
     *
     * @param string $key The key that identifies the stock
     * @param \Closure $filter You can provide a filter to change the
     * query builder behaviour when returning the results
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws \App\Exceptions\ContentNotFoundException
     */
    public function getStock($key, \Closure $filter = NULL)
    {
        $section = $this->child_sections()
            ->where('key', $key)
            ->where('type', 'stock')
            ->first();
        if(!$section) {
            throw new ContentNotFoundException("Couldn't find $key stock");
        }

        if($filter) {
            return $filter($section->contents());
        }

        return $section->contents()->get();
    }

    /**
     * Return subsections as a collection of Section elements
     *
     * Return only the child_sections that are of the types 'page' and
     * 'category'.
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getSubsections()
    {
        $subsections = $this->child_sections()
            ->whereIn('type', ['page', 'category'])
            ->get();
        return $subsections;
    }

    /**
     * Return a subsection with the given key
     *
     * Return a Section element that is a child_section of the 'page'
     * or 'category' type.
     *
     * @param string $key The key that identifies the subsection
     * @return \App\Section
     * @throws \App\Exceptions\SubsectionNotFoundException
     */
    public function getSubsection($key)
    {
        $subsection = $this->child_sections()
            ->where('key', $key)
            ->whereIn('type', ['page', 'category'])
            ->first();
        if(!$subsection) {
            throw new SubsectionNotFoundException(
                "Couldn't find $key subsection"
            );
        }
        return $subsection;
    }
}
