<?php

namespace App\Http\Controllers\CMS;

use App\Contracts\StructureManager;
use App\Http\Controllers\Controller;
use App\Section;
use Illuminate\Http\Request;

class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function main()
    {
        $sections = Section::where('section_id', null)->get();
        return view('cms.sections', ['sections' => $sections] );
    }

    /**
     * Display a list of subsections
     *
     * @param \App\Section $parent
     */

    public function index(Section $parent)
    {
        $sections = $parent->child_sections()->whereIn('type', ['page', 'category'])->get();
        return view('cms.sections', ['parent' => $parent, 'sections' => $sections]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param \App\Section $parent
     * @return \Illuminate\Http\Response
     */
    public function create(Section $parent)
    {
        return view('cms.structure.section', ['mode' => 'create', 'parent' => $parent]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Section $parent
     * @param  \Illuminate\Http\Request  $request
     * @param \App\Contracts\StructureManager $structure
     * @return \Illuminate\Http\Response
     */
    public function store(Section $parent, Request $request, StructureManager $structure)
    {
        $config = $structure->getNewSubsection($parent);

        $section = new Section;
        $section->name = $request->input('name');
        $section->slug = $this->generateSlug($parent, $section->name);
        $section->key = ':new';
        $section->type = $config[':type'];
        $parent->child_sections()->save($section);

        $content = $config[':content'];
        foreach($content as $name => $config) {
            $type = 'content';
            if(key_exists(':stock', $config) && $config[':stock']) {
                $type = 'stock';
            }

            $content_section = new Section;
            $content_section->name = ucfirst($name);
            $content_section->slug = $this->generateSlug($section, $name);
            $content_section->type = $type;
            $content_section->key = $name;
            $section->child_sections()->save($content_section);
        }

        return redirect()->action('CMS\SectionController@index', ['parent' => $parent]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section $parent
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $parent, Section $section)
    {
        return view('cms.structure.section', ['mode' => 'edit', 'parent' => $parent, 'section' => $section]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Section $parent
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Section $parent, Section $section)
    {
        if($section->name != $request->input('name')) {
            $section->name = $request->input('name');
            $section->slug = $this->generateSlug($parent, $section->name);
            $section->save();
        }

        return redirect()->action('CMS\SectionController@index', ['parent' => $parent]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $parent
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $parent, Section $section)
    {
        $section->delete();
        return redirect()->action('CMS\SectionController@index', ['parent' => $parent]);
    }

    /**
     * Generates a unique slug for Section
     *
     * @param  \App\Section $section
     * @param  string $name
     * @return string
     */
    protected function generateSlug($section, $name) {
        $original_slug = $section->slug.'-'.str_slug($name, '-');
        $slug = $original_slug;
        $count = 0;
        while (Section::where('slug', $slug)->count() > 0) {
            $count++;
            $slug = $original_slug.'-'.$count;
        }

        return $slug;
    }
}
