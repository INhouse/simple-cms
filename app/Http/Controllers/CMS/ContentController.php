<?php

namespace App\Http\Controllers\CMS;

use App\Http\Controllers\Controller;
use App\Http\Middleware\CMS\AddContentName;
use App\Contracts\StructureManager;
use App\Section;
use App\Content;
use App\Field;
use App\Facades\MediaManager;
use Illuminate\Http\Request;

class ContentController extends Controller
{
    /**
     * Asigns middleware to the controller
     *
     * @return void
     */
    public function __construct() {
        $this->middleware(AddContentName::class)->only(['store', 'update']);
    }

    /**
     * Display a listing of the resource.
     *
     * @param  \App\Section  $section
     * @return \Illuminate\Http\Response
     */
    public function index(Section $section)
    {
        if($section->type == 'page' || $section->type == 'category') {
            return view('cms.content_sections', ['section' => $section]);
        }
        elseif($section->type == 'stock') {
            return view('cms.contents', ['section' => $section]);
        }
        elseif($section->type == 'content') {
            if($section->contents()->count() > 0) {
                $content = $section->contents()->first();
                return redirect(action('CMS\ContentController@edit', ['section' => $section, 'content' => $content]));
            }
            else {
                return redirect(action('CMS\ContentController@create', ['section' => $section]));
            }
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  \App\Section         $section
     * @param  \App\Contracts\StructureManager $structure
     * @return \Illuminate\Http\Response
     */
    public function create(Section $section, StructureManager $structure)
    {
        $fields = $structure->getFields($section);
        return view('cms.editor.form', ['mode' => 'create', 'section' => $section, 'fields' => $fields]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Section  $section
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Section $section, Request $request)
    {
        $content = new Content();
        $content->name = $request->input('name');
        $content->slug = $this->generateSlug($section, $request->input('name'));
        $content->section()->associate($section);
        $content->save();

        $fields = $request->input('field.*');
        foreach($fields as $field_name_raw) {
            $field_name = str_slug($field_name_raw);
            if($request->has('value.'.$field_name)
               && !is_null($request->input('value.'.$field_name))) {
                $field = new Field();
                $field->name = $field_name_raw;
                $field->type = $request->input('type.'.$field_name);
                $field->value = $request->input('value.'.$field_name);
                $content->fields()->save($field);
            }
            else {
                $field = new Field();
                $field->name = $field_name_raw;
                $field->type = $request->input("type.$field_name");
                $field->value = '';
                $content->fields()->save($field);
            }
        }

        if($request->has('image')) {
            MediaManager::storeImages($content->images(), $request->input('image.*'), $request);
        }

        if($request->has('document')) {
            MediaManager::storeDocuments($content->documents(), $request->input('document.*'), $request);
        }

        if($request->has('gallery')) {
            foreach($request->input('gallery.*') as $gallery_name) {
                $gallery_type = $request->input('gallery_type.'.$gallery_name);
                $elements = [];
                if($gallery_type == 'images') {
                    $elements = $request->input('gallery_image.'.$gallery_name.'.*');
                }
                elseif($gallery_type == 'documents') {
                    $elements = $request->input('gallery_document.'.$gallery_name.'.*');
                }
                $gallery = MediaManager::storeGallery($gallery_name, $gallery_type, $elements, $request);
                $content->galleries()->attach($gallery);
            }
        }

        if($section->type == 'stock') {
            return redirect()->action('CMS\ContentController@index', ['section' => $section]);
        }

        return redirect()->action('CMS\ContentController@index', ['section' => $section->parent_section]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Section  $section
     * @param  \App\Contracts\StructureManager $structure
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function edit(Section $section, StructureManager $structure, Content $content)
    {
        $images = [];
        foreach($content->images as $image) {
            $images[$image->label] = $image;
        }

        $documents = [];
        foreach($content->documents as $document) {
            $documents[$document->label] = $document;
        }

        $galleries = [];
        foreach($content->galleries as $gallery) {
            if($gallery->type == 'images') {
                $galleries[$gallery->name] = $gallery->images;
            }
            elseif($gallery->type == 'documents') {
                $galleries[$gallery->name] = $gallery->documents;
            }
        }

        $fields_template = $structure->getFields($section);

        $fields = [];
        foreach($content->fields as $field) {
            $fields[$field->form_name] = $field;
            $fields[$field->form_name]->details = $fields_template[$field->form_name]->details;
        }

        foreach($fields_template as $field) {
            if(!array_key_exists($field->form_name, $fields)) {
                $fields[$field->form_name] = $field;
            }
        }

        return view('cms.editor.form', ['mode' => 'update', 'section' => $section, 'content' => $content, 'fields' => $fields, 'images' => $images, 'documents' => $documents, 'galleries' => $galleries]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Section  $section
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function update(Section $section, Request $request, Content $content)
    {
        if ($request->has('slug')) {
            $content->slug = $request->input('slug');
        }
        elseif ($content->name != $request->input('name')) {
            $content->slug = $this->generateSlug($section, $request->input('name'));
        }
        $content->name = $request->input('name');
        $content->save();

        $content_fields = [];
        foreach( $content->fields as $field ) {
            $content_fields[$field->form_name] = $field;
        }

        $fields = $request->input('field.*');
        foreach($fields as $field_name_raw) {
            $field_name = str_slug($field_name_raw);
            if(array_key_exists($field_name, $content_fields)) {
                $field = $content_fields[$field_name];
            }
            else {
                $field = new Field();
                $field->content_id = $content->id;
                $field->name = $field_name_raw;
                $field->type = $request->input("type.$field_name");
            }

            if($request->input("value.$field_name")) {
                $field->value = $request->input("value.$field_name");
                $field->save();
            }
            else {
                $field->value = '';
                $field->save();
            }
        }

        if($request->has('delete_image')){
            foreach($request->input('delete_image.*') as $image_label) {
                MediaManager::deleteImage($content->images(), $image_label);
            }
        }

        if($request->has('image')) {
            MediaManager::storeImages($content->images(), $request->input('image.*'), $request);
        }

        if($request->has('delete_document')) {
            foreach($request->input('delete_document.*') as $document_label) {
                MediaManager::deleteDocument($content->documents(), $document_label);
            }
        }

        if($request->has('document')) {
            MediaManager::storeDocuments($content->documents(), $request->input('document.*'), $request);
        }

        $galleries = [];
        foreach($content->galleries as $gallery) {
            $galleries[$gallery->name] = $gallery;
        }

        if($request->has('gallery')) {
            foreach($request->input('gallery.*') as $gallery_name) {
                if(key_exists($gallery_name, $galleries)) {
                    $gallery = $galleries[$gallery_name];
                    if($request->has('gal_delete_image.'.$gallery_name)){
                        foreach($request->input('gal_delete_image.'.$gallery_name.'.*') as $image_label) {
                            MediaManager::deleteImage($gallery->images(), $image_label);
                        }
                    }
                    if($request->has('gal_delete_document.'.$gallery_name)) {
                        foreach($request->input('gal_delete_image.'.$gallery_name.'.*') as $document_label) {
                            MediaManager::deleteDocument($gallery->documents(), $document_label);
                        }
                    }
                    if($gallery->type == 'images') {
                        $elements = $request->input('gallery_image.'.$gallery_name.'.*');
                        MediaManager::storeImages($gallery->images(), $elements, $request);
                    }
                    elseif($gallery->type == 'documents') {
                        $elements = $request->input('gallery_document.'.$gallery_name.'.*');
                        MediaManager::storeDocuments($gallery->documents(), $elements, $request);
                    }
                }
                else {
                    $gallery_type = $request->input('gallery_type.'.$gallery_name);
                    $elements = [];
                    if($gallery_type == 'images') {
                        $elements = $request->input('gallery_image.'.$gallery_name.'.*');
                    }
                    elseif($gallery_type == 'documents') {
                        $elements = $request->input('gallery_document.'.$gallery_name.'.*');
                    }
                    $gallery = MediaManager::storeGallery($gallery_name, $gallery_type, $elements, $request);
                    $content->galleries()->attach($gallery);
                }
            }
        }

        if($section->type == 'stock') {
            return redirect()->action('CMS\ContentController@index', ['section' => $section]);
        }

        return redirect()->action('CMS\ContentController@index', ['section' => $section->parent_section]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Section  $section
     * @param  \App\Content  $content
     * @return \Illuminate\Http\Response
     */
    public function destroy(Section $section, Content $content)
    {
        $content->delete();

        return redirect()->action('CMS\ContentController@index', ['section' => $section]);
    }

    /**
     * Generates a unique slug for Content
     *
     * @param  \App\Section $section
     * @param  string $name
     * @return string
     */
    protected function generateSlug($section, $name) {
        $original_slug = $section->id.'--'.str_slug($name, '-');
        $slug = $original_slug;
        $count = 0;
        while (Content::withTrashed()->where('slug', $slug)->count() > 0) {
            $count++;
            $slug = $original_slug.'-'.$count;
        }

        return $slug;
    }
}
