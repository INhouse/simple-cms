<?php

namespace App\Http\Middleware\CMS;

use Closure;

class AddContentName
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name_value = '';

        // TODO (Javs) Estos valores deben de modificarse al aplicar localización
        if($request->has('value.nombre')) {
            $name_value = $request->input('value.nombre');
        }
        elseif($request->has('value.titulo')) {
            $name_value = $request->input('value.titulo');
        }
        else {
            $name_value = $request->input('value.*')[0];
        }

        if($name_value != '') {
            $request->merge(['name' => $name_value]);
        }

        return $next($request);
    }
}
