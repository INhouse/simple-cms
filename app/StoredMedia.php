<?php

namespace App;

use App\Events\MediaDeleted;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

abstract class StoredMedia extends Model
{
    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $events = [
        'deleted' => MediaDeleted::class,
    ];

    protected $appends = ['url'];

    public function has_media()
    {
        return $this->morphTo();
    }

    public function getUrlAttribute() {
        return $this->attributes['url'] = Storage::url($this->attributes['file']);
    }
}
