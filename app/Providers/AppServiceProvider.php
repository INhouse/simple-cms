<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public $bindings = [
        \App\Contracts\ImageEditor::class => \App\Services\CropperImageEditor::class,
        \App\Contracts\MediaManager::class => \App\Services\MediaManager::class,
        \App\Contracts\StructureManager::class => \App\Services\ArrayStructureManager::class,
    ];

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
