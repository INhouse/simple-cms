<?php

namespace App;

class Image extends StoredMedia
{
    protected $table = 'media_images';

    protected $appends = ['url', 'original'];

    public function getUrlAttribute() {
        return $this->attributes['url'] = route('imagecache', [
            'template' => 'default',
            'filename' => $this->attributes['cropped']?$this->attributes['cropped']:$this->attributes['file'],
        ]);
    }

    public function getOriginalAttribute() {
        return $this->attributes['original'] = route('imagecache', [
            'template' => 'original',
            'filename' => $this->attributes['file'],
        ]);
    }

    public function getUrlFilter($filter) {
        return route('imagecache', [
            'template' => $filter,
            'filename' => $this->attributes['file'],
        ]);
    }
}
