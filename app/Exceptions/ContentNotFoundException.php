<?php

namespace App\Exceptions;

use Exception;

/**
 * An exception when the requested content doesn't exists
 */

class ContentNotFoundException extends Exception
{
    //
}
