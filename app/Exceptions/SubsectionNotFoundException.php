<?php

namespace App\Exceptions;

use Exception;

/**
 * An exception when the requested subsection doesn't exists
 */

class SubsectionNotFoundException extends Exception
{
    //
}
