<?php

namespace App\Traits;

trait HasMedia
{
    public function images()
    {
        return $this->morphMany('App\Image', 'has_media');
    }

    public function documents()
    {
        return $this->morphMany('App\Document', 'has_media');
    }
}
