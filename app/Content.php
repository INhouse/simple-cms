<?php

namespace App;

use App\Traits\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Contains the content elements for the site
 *
 * @property int $id Primary key
 * @property int $section_id Foreing key to the belonging Section
 * @property string $name Descriptive name for the content in the CMS
 * @property string $slug Slug identifier for urls
 *
 * @property \Illuminate\Database\Eloquent\Collection $fields
 * Fields that belong to the content
 * @property App\Section $section
 * Section the content belongs to
 * @property \Illuminate\Database\Eloquent\Collection $galleries
 * Collection of galleries that belong to the content
 *
 * @property \Carbon\Carbon $created_at Date of creation
 * @property \Carbon\Carbon $updated_at Date of last update
 * @property \Carbon\Carbon $deleted_at Date of soft deletion
 */
class Content extends Model
{
    use SoftDeletes;
    use HasMedia;

    /**
     * @var string Table name
     */
    protected $table = 'cms_contents';

    /**
     * @var array List of relationships that are eager loaded
     */
    protected $with = ['fields', 'images'];


    /**
     * Define the property that is used to identify the model on routes
     *
     * @return string The property name
     */
    public function getRouteKeyName()
    {
        return 'slug';
    }

    /**
     * Return the relationship for fields
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function fields() {
        return $this->hasMany('App\Field');
    }

    /**
     * Define a search scope `whereField` for the query filters
     *
     * Simplifies filtering a Content element by the values on its
     * Fields, by abstracting a compound query that is added to the
     * builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query Query builder
     * @param string $field Name of the filtered field
     * @param mixed $value Expected value for the filtered field
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereField($query, $field, $value) {
        return $query->whereHas('fields', function($q) use ($field, $value) {
            $q->where('name', $field)->where('value', $value);
        });
    }

    /**
     * Define a search scope `whereField` for the query filters
     *
     * Simplifies filtering a Content element by the values on its
     * Fields using a `like` comparison, by abstracting a compound
     * query that is added to the builder.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query Query builder
     * @param string $field Name of the filtered field
     * @param mixed $match Expression the be matched against the like
     * keyword on the database
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeWhereFieldLike($query, $field, $match) {
        return $query->whereHas('fields', function($q) use ($field, $match) {
            $q->where('name', $field)->where('value', 'like', $match);
        });
    }

    /**
     * Return the relationship to the parent Section
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function section() {
        return $this->belongsTo('App\Section');
    }

    /**
     * Return the value of the field with the given name
     *
     * @param string $name Field name
     * @return string Field value
     */
    public function getField($name) {
        $field = $this->fields()->where('name', $name)->first();
        if($field) {
            return $field->value;
        }
        else {
            return '';
        }
    }

    /**
     * Return an Image object with the given name
     *
     * @param string $name Name of the image field
     * @return App\Image
     */
    public function getImg($name) {
        $image = $this->images()->where('label', $name)->first();
        return $image;
    }

    /**
     * Return the default url for the image with the given name
     *
     * @param string $name Name of the image field
     * @return string
     */
    public function getImgUrl($name) {
        $image = $this->images()->where('label', $name)->first();
        if($image) {
            return $image->url;
        }
        else {
            return '';
        }
    }

    /**
     * Return the Document object with the given name
     *
     * @param string $name Name for the document field
     * @return App\Document
     */
    public function getFile($name) {
        $file = $this->documents()->where('label', str_slug($name))->first();
        return $file;
    }

    /**
     * Return the default url for the document with the given name
     *
     * @param string $name Name for the document field
     * @return string
     */
    public function getFileUrl($name) {
        $file = $this->documents()->where('label', str_slug($name))->first();
        if($file) {
            return $file->url;
        }
        else {
            return '';
        }
    }

    /**
     * Return the relationship to the internal galleries
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function galleries() {
        return $this->belongsToMany('App\Gallery', 'cms_contents_media_galleries', 'cms_contents_id', 'media_galleries_id');
    }
}
