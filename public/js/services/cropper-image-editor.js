(function(){
    "use strict";

    document.addEventListener("DOMContentLoaded", function(){
        const images = document.getElementsByClassName('cropper-editor');
        for (let image of images) {
            let label = image.getAttribute('data-label');
            let width = document.getElementsByName('image_editor[' + label + '][width]')[0];
            let height = document.getElementsByName('image_editor[' + label + '][height]')[0];
            let x = document.getElementsByName('image_editor[' + label + '][x]')[0];
            let y = document.getElementsByName('image_editor[' + label + '][y]')[0];
            let aspect_ratio = Number(image.getAttribute('data-aspect-ratio'));
            if(aspect_ratio === 0) {
                aspect_ratio = NaN;
            }

            let data = {
                rotate: 0,
                width: Number(width.value),
                height: Number(height.value),
                x: Number(x.value),
                y: Number(y.value),
            };

            let cropper = new Cropper(image, {
                viewMode: 1,
                aspectRatio: aspect_ratio,
                movable: false,
                scalable: false,
                zoomable: false,
                rotatable: false,
                dragMode: 'move',
                autoCrop: false,
                crop(event) {
                    width.value = event.detail.width;
                    height.value = event.detail.height;
                    x.value = event.detail.x;
                    y.value = event.detail.y;
                },
                ready() {
                    cropper.crop();
                    cropper.setData(data);
                },
            });
        }
    });

})();
