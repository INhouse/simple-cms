@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2>Contenido para {{ $section->name }}</h2>
            <div class="list-group">
                <a class="list-group-item" href="{{ action('CMS\ContentController@create', ['section' => $section]) }}"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Agregar contenido</a>
                @forelse ($section->contents()->latest()->get() as $content)
                    <a class="list-group-item" href="{{ action('CMS\ContentController@edit', ['section' => $section, 'content' => $content]) }}">{{ $content->name }}
                        <span class="btn pull-right" onclick="event.preventDefault(); document.getElementById('delete-form-{{ $content->id }}').submit();" ><span class="glyphicon glyphicon-trash"></span></span>
                        <form id="delete-form-{{ $content->id }}" action="{{ action('CMS\ContentController@destroy', ['section' => $section, 'content' => $content])}}" method="POST">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                        </form>
                    </a>
                @empty
                    No hay contenido en esta sección
                @endforelse
            </div>
            <a href="{{ ($section->parent_section) ? action('CMS\ContentController@index', ['section' => $section->parent_section]) : route('cms') }}">Regresar</a>
        </div>
    </div>
</div>
@endsection
