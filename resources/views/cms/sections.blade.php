@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2>Secciones</h2>
            @if(isset($parent))
                <h3>{{ $parent->name }}</h3>
            @endif
            <div class="list-group">
            @forelse ($sections as $section)
                <li class="list-group-item">
                    {{ $section->name }}<br/>
                    @if ($section->child_sections()->whereIn('type', ['content', 'stock'])->count() > 0)
                    <a class="btn btn-default" href="{{ action('CMS\ContentController@index', ['section' => $section]) }}">Ver Contenido</a>
                    @endif

                    @if ($section->child_sections()->whereIn('type', ['page', 'category'])->count() > 0)
                        <a class="btn btn-default" href="{{ action('CMS\SectionController@index', ['parent' => $section]) }}">Ver Subsecciones</a>
                    @endif

                    @if ($section->type == 'category')
                    <a class="btn btn-default" href="{{ action('CMS\SectionController@create', ['parent' => $section]) }}">Agregar Subsección</a>
                    @endif

                    @if (isset($parent))
                    <a class="btn btn-sm btn-warning" href="{{ action('CMS\SectionController@edit', ['parent' => $parent, 'section' => $section]) }}">Editar</a>
                    <form class="form-inline" style="display: inline" method="POST" action="{{ action('CMS\SectionController@destroy', ['parent' => $parent, 'section' => $section]) }}">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button class="btn btn-sm btn-danger btn-delete" {{ ($section->child_sections()->count() == 0 && $section->contents()->count() == 0)?'':'disabled' }}>Eliminar</button>
                    </form>
                    @endif
                </li>
            @empty
                <p>No hay secciones para agregar contenido</p>
            @endforelse
            </div>

            @if(isset($parent))
                <a href="{{ ($parent->parent_section) ? action('CMS\SectionController@index', ['parent' => $parent->parent_section]) : route('cms') }}">Regresar</a>
            @endif
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $('.btn-delete').on('click', function(e){
        var conf = confirm('¿Está seguro que desea eliminar este elemento?');
        if(!conf) {
            e.preventDefault();
        }
    })
</script>
@endpush
