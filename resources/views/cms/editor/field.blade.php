<div class="form-group">
    <label class="text-capitalize">
        @if ($field->type == 'checkbox')
            <input type="hidden" name="value[{{ $field->form_name }}]" value="0"/>
            <input type="checkbox" name="value[{{ $field->form_name }}]" value="1" {{ $field->value?'checked':'' }}/>
        @endif
        {{ $field->name }}
    </label>
    <input type="hidden" name="field[]" value="{{ $field->name }}" />
    <input type="hidden" name="type[{{ $field->form_name }}]" value="{{ $field->type }}" />
    @if ($field->type == 'text')
        @include('cms.editor.input', ['type' => 'text'])
    @elseif ($field->type == 'number')
        @include('cms.editor.input', ['type' => 'number'])
    @elseif ($field->type == 'price')
        <div class="input-group">
            <span class="input-group-addon">$</span>
            <input class="form-control" type="number" name="value[{{ $field->form_name }}]" min="0" step="0.01" value="{{ $field->value }}" />
        </div>
    @elseif ($field->type == 'textarea')
        <textarea class="form-control" name="value[{{ $field->form_name }}]">{{ $field->value }}</textarea>
    @elseif ($field->type == 'video')
        @include('cms.editor.input', ['type' => 'url'])
    @elseif($field->type == 'image')
        @includeIf('cms.media.imageupload', ['name' => $field->form_name, 'aspect_ratio' => $field->details])
    @elseif ($field->type == 'document')
        @includeIf('cms.media.documentupload', ['name' => $field->form_name])
    @elseif ($field->type == 'image_gallery')
        @includeIf('cms.media.gallery', ['name' => $field->form_name, 'type' => 'images'])
    @elseif ($field->type == 'document_gallery')
        @includeIf('cms.media.gallery', ['name' => $field->form_name, 'type' => 'documents'])
    @endif
</div>
