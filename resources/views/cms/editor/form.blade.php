@extends('layouts.app')

@inject('image_editor', 'App\Contracts\ImageEditor')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if ($mode == 'create')
                <form action="{{ action('CMS\ContentController@store', ['section' => $section]) }}" enctype="multipart/form-data" method="POST">
            @else
                <form action="{{ action('CMS\ContentController@update', ['section' => $section, 'content' => $content]) }}" enctype="multipart/form-data" method="POST">
                {{ method_field('PUT') }}
            @endif
                @foreach($fields as $field)
                    @include('cms.editor.field')
                @endforeach
                {{ csrf_field() }}
                <button class="btn btn-main">Save</button>
            </form>
            @if($section->type == 'stock')
            <a href="{{ action('CMS\ContentController@index', ['section' => $section]) }}">Regresar</a>
            @else
            <a href="{{ action('CMS\ContentController@index', ['section' => $section->parent_section]) }}">Regresar</a>
            @endif
        </div>
    </div>
</div>
@endsection

@push('scripts')
{!! $image_editor->initialize() !!}
@endpush
