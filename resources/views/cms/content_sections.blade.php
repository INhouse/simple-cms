@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2>Contenido para {{ $section->name }}</h2>
            <div class="list-group">
                @forelse ($section->child_sections()->whereIn('type', ['content', 'stock'])->get() as $content)
                    <a class="list-group-item" href="{{ action('CMS\ContentController@index', ['section' => $content]) }}">{{ $content->name }}</a>
                @empty
                    No hay contenido en esta sección
                @endforelse
            </div>
            <a href="{{ ($section->parent_section) ? action('CMS\SectionController@index', ['parent' => $section->parent_section]) : route('cms') }}">Regresar</a>
        </div>
    </div>
</div>
@endsection
