@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            @if ($mode == 'create')
                <form action="{{ action('CMS\SectionController@store', ['parent' => $parent]) }}" enctype="multipart/form-data" method="POST">
            @else
                <form action="{{ action('CMS\SectionController@update', ['parent' => $parent,'section' => $section]) }}" enctype="multipart/form-data" method="POST">
                {{ method_field('PUT') }}
            @endif
                <div class="form-group">
                    <label>Nombre</label>
                    <input type="text" class="form-control" name="name" value="{{ isset($section)?$section->name:'' }}" />
                </div>
                {{ csrf_field() }}
                <button class="btn btn-main">Guardar</button>
            </form>
            @if($mode == 'create')
            <a href="{{ ($parent->parent_section) ? action('CMS\SectionController@index', ['parent' => $parent->parent_section]) : route('cms') }}">Regresar</a>
            @else
            <a href="{{ action('CMS\SectionController@index', ['parent' => $parent]) }}">Regresar</a>
            @endif
        </div>
    </div>
</div>
@endsection
