<input type="hidden" name="value[{{ $name }}]" value="upload"/>
<input type="hidden" name="document[]" value="{{ $name }}" />
@isset($documents[$name])
    <br/><a href="{{ asset($documents[$name]->url) }}">{{ $documents[$name]->name }}</a>
    <div class="checkbox">
        <label>
            <input type="checkbox" name="delete_document[]" value="{{ $name }}" />
            Eliminar?
        </label>
    </div>
    <span>Reemplazar documento: </span>
@endisset
@include('cms.media.fileupload', ['name' => 'document_'.$name, 'accept' => '.pdf,.doc,.docx,.odt,.ppt,.pptx,.odp'])
