<input type="hidden" name="gallery[]" value="{{ $name }}" />
<input type="hidden" name="value[{{ $name }}]" value="gallery"/>
<input type="hidden" name="gallery_type[{{ $name }}]" value="{{ $type }}" />
<div id="template-gallery-{{ $name }}" hidden>
    @include('cms.media.gallery_element', ['value' => '@{{value}}'])
</div>
@if(isset($galleries[$name]))
    @foreach($galleries[$name] as $element)
        @include('cms.media.gallery_element', ['value' => $element->label])
    @endforeach
    <input type="hidden" name="gallery_count_{{ $name }}" value="{{ count($galleries[$name]) }}" />
@else
    @include('cms.media.gallery_element', ['value' => 'gallery_'.$name.'_0'])
    <input type="hidden" name="gallery_count[{{ $name }}]" value="1" />
@endif
<a href="#" class="agregar-elemento-{{ $name }}" data-gallery="{{ $name }}">Agregar</a>

@push('scripts')
    <script type="text/javascript">
        (function(){
            'use strict';

            $('.agregar-elemento-{{ $name }}').on('click', function(event){
                event.preventDefault();
                var gallery_name = $(this).data('gallery');
                var $count = $('input').filter('[name="gallery_count_' + gallery_name + '"]');
                var count = $count.val();
                var html = $('#template-gallery-' + gallery_name).html();
                var value = 'gallery_' + gallery_name + '_' + count;
                var check_id = '{{$name}}_' + value;
                var recount = 0;
                while( document.getElementById(check_id) !== null ) {
                    recount++;
                    value = 'gallery_' + gallery_name + '_' + recount;
                    check_id = '{{$name}}_' + value;
                }
                html = html.replace(/@{{value}}/g, value);
                $(this).before(html);
                $count.val(parseInt(count) + 1);
            });
        })();
    </script>
@endpush
