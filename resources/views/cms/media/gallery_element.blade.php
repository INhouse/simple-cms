<div id="{{ $name }}_{{ $value }}">
@if($type == 'images')
    <input type="hidden" name="gallery_image[{{ $name }}][]"  value="{{ $value }}" />
    @if(isset($element))
        <div><img src="{{ asset($element->url) }}" alt="{{ $element->name }}" /></div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="gal_delete_image[{{ $name }}][]" value="{{ $value }}" />
                Eliminar?
            </label>
        </div>
    @else
        <input type="file" name="image_{{ $value }}" accept="image/*"/>
    @endif
@elseif($type == 'documents')
    <input type="hidden" name="gallery_document[{{ $name }}][]" value="{{ $value }}" />
    @if(isset($element))
        <div><a href="{{ asset($element->url) }}">{{ $element->name }}</a></div>
        <div class="checkbox">
            <label>
                <input type="checkbox" name="gal_delete_document[{{ $name }}][]" value="{{ $element->label }}" />
                Eliminar?
            </label>
        </div>
    @else
        <input type="file" name="document_{{ $value }}" accept=".pdf,.doc,.docx,.odt,.ppt,.pptx,.odp" />
    @endif
@endif
</div>
<br/>
