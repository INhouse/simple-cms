<input type="hidden" name="value[{{ $name }}]" value="upload"/>
<input type="hidden" name="image[]" value="{{ $name }}" />
@isset($images[$name])
    <br/>
    {!! $image_editor->createEditor($images[$name], $aspect_ratio) !!}
    <div class="checkbox">
        <label>
            <input type="checkbox" name="delete_image[]" value="{{ $name }}" />
            Eliminar?
        </label>
    </div>
    <span>Reemplazar imagen:</span>
@endisset
@include('cms.media.fileupload', ['name' => 'image_'.$name, 'accept' => 'image/*'])
