<?php

/**
 * Define the site structure
 */

return [
    'site' => [
        // Start main sections definition

        // Basic homepage
        'home' => [
           ':name' => 'Home',
           ':type' => 'page',
           ':content' => [
                'information' => [
                    'title' => 'text',
                    'body' => 'textarea',
                ],
            ],
        ],

        // End sections definition
    ],
];
