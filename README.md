# Simple CMS

This project is a Content Management System built on top of Laravel that works
as a boilerplate for a managed website.

The CMS offers a multi purpose database that allows to store different kinds of
content in a different array of structures, meant to be used as the backend for
the website.

The database stores the information in two main categories:

* **Sections**: this are the structural containers in the website. A section
can be a page, a category on the website, or a page fragment.
* **Contents**: a `Content` is a container unit that is meant to store
information of some kind. It can be an article, a product, a gallery element,
etc. The shape of this `Content` is determined by the `Fields` it contains.

The CMS structure is defined in the `structure.php` configuration file. In this
file you can map the sections, include content elements and define their fields.

To create a website with the CMS you are required to have a basic knowledge of
Laravel. The system provides an administration panel for the CMS, and allows
the define the structure and contents for the site, but you have to create the
templates and controllers needed to render the website.

You can learn more about Laravel on https://laravel.com/docs.

## Installation and Configuration

This project contains the boilerplate of a website built with Laravel.

### Dependencies

To install it, first you need to install its dependencies with
`composer install`. To avoid any conflicts between versions, we
**DO NOT recommend** to run the _composer update_ command.

### Configuration

A `.env` file is required to manage the site configuration. For a quick
configuration in development you can copy the `.env.example` file with
`cp .env.example .env`. This has the default configuration values needed for
development. You can edit the file as you wish with your specific values.

For a more production oriented configuration, you can use the
`.env.production.example` file as a reference.

### Database

If you are using the default development configuration, a sqlite database will
be used for the website. You will need to create an empty file in
`database/database.sqlite` before running the migration commands.

The database structure to the CMS can be created with the `php artisan migrate`
command. We also recommend that you seed an administrator user with the
`php artisan db:seed` command, this will create a default user with email
_admin@example.com_ and password _password_. You can edit this default user
values in `database/seeds/UsersTableSeeder.php`.

### CMS Structure

The CMS structure is managed on the `structure.php` configuration file. To
create the structure in the database you have to run
`php artisan build:sections`. This command will check the structure defined in
the configuration and create the _Section_ and _Content_ elements required.

If you modify the `structure.php` file, you will have to update the
configuration cache with `php artisan config:cache` and then run
`php artisan build:sections` again. This command is non-destructive, it will
add any new sections and will modify the properties of an existing section, but
will not delete any section that no longer exists and will duplicate sections
that have been fully renamed.

If your are only changing the _Fields_ on a _Content_ definition, you don't
need to run the `build:sections` command, just updating the configuration with
`php artisan config:cache` will be enough to show the new fields on the forms.

#### Structure Definition

The structure is defined as an array. All sections belong to a main _site_
node. The structure is mainly defined as _sections_ and _content_.

_Sections_ can be added on the main _site_ or in any _subsections_ structure. A
default section is:

```php
    'home' => [ // This is the "key" to the section and is used in the url slug

        ':name' => 'Home', // Section name, mainly used in the CMS panel

        ':type' => 'page', // Can be "page" or "category"
        // "page" is used for section that only have content
        // "category" can have subsections

        ':content' => [ // Define the Content elements

            'banner' => [ // Content "key", will be used on the url slug

                ':name' => 'Banner',
                // Content name
                // If ommited a capitalized version of the key will be used

                ':stock' => true,
                // When stock is true, multiple elements will be managed
                // Default is false

                ':fields' => [
                    // These fields will be used in the element form
                    // The syntax is 'field-name' => 'type'
                    // Supported types are text, textarea, checkbox, number,
                    // price, video, image, document, image_gallery,
                    // document_gallery

                    'banner' => 'image=600:400',
                    // Image fields can define an aspect ratio
                    // like image=width:height

                    'description' => 'textarea',
                    'link' => 'text'
                ]
            ],

            'information' => [
                // You can also omit the other properties
                // and define only the fields
                'title' => 'text',
                'description' => 'textarea',
            ],
        ],
    ],
```

A section that is a _category_ will be able to define subsections, and even
define templates for new subsections that will be created on the future:

```php
    'products' => [
        ':name' => 'Products',
        ':type' => 'category',

        ':template' => [
            // You can define a content template to be used in subsections
            'name' => 'text',
            'description' => 'textarea',
            'cover' => 'image',
            'gallery' => 'image_gallery'
        ],

        ':content' => [
            // Categories can also have content defined, but is not required
            'description' => [
                'title' => 'text',
                'description' => 'textarea'
            ],
        ],

        ':subsections' => [
            // Sections defined in subsection can have the same properties as
            // the ones in the main site, they can even be categories
            'shirts' => [
                ':name' => 'Shirts',
                ':type' => 'page',
                ':content' => [
                    'description' => [
                        'title' => 'text',
                        'description' => 'textarea'
                    ],
                    'products' => [
                        ':stock' => true,
                        ':fields' => ':template'
                        // Here you reference to the fields defined earlier
                    ]
                ]
            ],
            // :new is a special key. Is used to define the properties for
            // subsections created by the end user in the CMS
            ':new' => [
                ':name' => '', // The name is left empty for :new sections
                ':type' => 'category', // You nest :new sections
                ':content' => [
                    'description' => [
                        'title' => 'text',
                        'description' => 'textarea'
                    ]
                ],
                ':subsections' => [
                    ':new' => [
                        ':name' => '',
                        ':type' => 'page',
                        ':content' => [
                            'description' => [
                                'title' => 'text',
                                'description' => 'textarea'
                            ],
                            'products' => [
                                ':stock' => true,
                                ':fields' => ':template'
                            ]
                        ]
                    ]
                ]
            ]
        ]
    ]
```

#### Shortcut Methods

The Section model has been extended to provide some useful shortcuts to the
structure elements. This methods simplify the process to navigate the structure
defined in the `structure.php` file.

##### Access Content elements

If you have a Section element that correspond to this structure:

```php
    'home' => [
        ':name' => 'Home',
        ':type' => 'page',
        ':content' => [
            'information' => [
                'title' => 'text',
                'description' => 'textarea',
                'cover' => 'image',
            ],
        ],
    ],
```

You can use the `getContent()` method to access the information element, and
later use the `getField()` method to obtain the _title_ and _description_.

```php
$home = Section::where('slug', 'home')->first();
$information = $home->getContent('information');
$title = $information->getField('title');
$description = $information->getField('description');
```

Images use different methods to directly access the image url, or to access the
`Image` object to access some additional properties.

```php
$image_url = $information->getImgUrl('cover');
$image = $information->getImg('cover');
$image_url = $image->url;
$image_original_url = $image->original;
```

You can also apply [Image Cache](http://image.intervention.io/use/cache)
filters using the `getUrlFilter()` method from the `Image` model.

You can also access a stock of elements with the `getStock()` Section method.
For a structure like this:

```php
    'home' => [
        ':name' => 'Home',
        ':type' => 'page',
        ':content' => [
            'banner' => [
                ':name' => 'Banner',
                ':stock' => true,
                ':fields' => [
                    'banner' => 'image=600:400',
                    'description' => 'textarea',
                    'link' => 'text'
                ],
            ],
        ],
    ],
```

You can get the collection of stock elements like this:

```php
$home = Section::where('slug', 'home')->first();
$banners = $home->getStock('banner');

foreach($banners as $banner) {
    $banner_image = $banner->getImgUrl('banner');
    $decription = $banner->getField('description');
}
```

If you need additional filters to the stock instead of getting all the elements
listed, you can pass a Closure to manage the Query Builder:

```php
$home = Section::where('slug', 'home')->first();
$banners = $home->getStock('banner', function($contents) {
    return $contents->whereField('active', true)->get();
});

foreach($banners as $banner) {
    $banner_image = $banner->getImgUrl('banner');
    $decription = $banner->getField('description');
}
```

When filtering `Content` you have access to the `whereField($field, $value)`
and `whereFieldLike($field, $match)` methods that are able to access the
internal fields on a `Content` element.

##### Access Subsections

There are also shortcut methods to manage subsections. You can get a specific
subsection using the `getSubsection()` method. For this structure:

```php
    'products' => [
        ':name' => 'Products',
        ':type' => 'category',
        ':subsections' => [
            'shirts' => [
                ':name' => 'Shirts',
                ':type' => 'page',
                ],
            ],
        ],
    ],
```

You can access the subsection like this:

```php
$products = Section::where('slug', 'products')->first();
$shirts = $products->getSubsection('shirts');
```

You can also get a list of all subsetions with `getSubsections()`. If you have
a structure that allows to create new subsection elements with `:new`, you
should access the whole list of subsections, as getting individual sections
that are dynamically created is harder:


```php
    'products' => [
        ':name' => 'Products',
        ':type' => 'category',
        ':subsections' => [
            ':new' => [
                ':name' => '',
                ':type' => 'category',
                ':content' => [
                    'description' => [
                        'title' => 'text',
                        'description' => 'textarea'
                    ],
                ],
            ],
        ],
    ],
```

Subsections should be accessed like:

```php
$products = Section::where('slug', 'products')->first();
$subsections = $products->getSubsections();

foreach($subsections as $subsection) {
    $description = $subsection->getContent('description');
}
```
