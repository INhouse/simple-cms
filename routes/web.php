<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');

Route::group([
    'middleware' => ['auth', 'is_admin'],
    'prefix' => 'admin'
], function(){

    Route::get('/', 'Admin\DashboardController@index');

    Route::group([
        'namespace' => 'CMS',
        'prefix' => 'cms'
    ], function(){

        Route::get('/', 'SectionController@main')->name('cms');

        Route::resource('{parent}/sections', 'SectionController', ['except' => ['show']]);

        Route::resource('{section}/content', 'ContentController', ['except' => ['show']]);

    });
});
