<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCmsContentsMediaGalleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cms_contents_media_galleries', function (Blueprint $table) {
            $table->integer('cms_contents_id')->unsigned();
            $table->integer('media_galleries_id')->unsigned();
            $table->primary(['cms_contents_id', 'media_galleries_id'], 'contents_galleries_primary');
            $table->foreign('cms_contents_id')->references('id')->on('cms_contents');
            $table->foreign('media_galleries_id')->references('id')->on('media_galleries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cms_contents_media_galleries');
    }
}
