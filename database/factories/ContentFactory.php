<?php

use Faker\Generator as Faker;

/**
 * Create a default Content with a random name, slug, and creates an
 * new Section as the parent.
 */
$factory->define(App\Content::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'slug' => $faker->unique()->word,
        'section_id' => function() {
            return factory(App\Section::class)->create()->id;
        }
    ];
});

/**
 * Define a 'no_section' state that creates a Content element without
 * a parent Section.
 */
$factory->state(App\Content::class, 'no_section', [
    'section_id' => '',
]);
