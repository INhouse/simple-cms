<?php

use Faker\Generator as Faker;

/**
 * Create a Section element with a random name, slug and key, with a
 * 'page' type as default.
 */
$factory->define(App\Section::class, function (Faker $faker) {
    $keyword = $faker->unique()->word;

    return [
        'name' => $keyword,
        'slug' => str_slug($keyword),
        'type' => 'page',
        'key' => str_slug($keyword),
    ];
});

/**
 * Define a 'content' state that create a Section with a 'content' type.
 */
$factory->state(App\Section::class, 'content', [
    'type' => 'content',
]);

/**
 * This callback adds a single Content element to a newly created
 * Section with the 'content' state.
 */
$factory->afterCreatingState(App\Section::class, 'content',
    function($section, $faker){
        $section->contents()->save(factory(App\Content::class)->create([
            'section_id' => $section->id,
        ]));
    }
);

/**
 * Define a 'stock' state that create a Section with a 'stock' type.
 */
$factory->state(App\Section::class, 'stock', [
    'type' => 'stock',
]);

/**
 * This callback adds a collection of Content elements to a newly
 * created Section with the 'stock' state.
 */
$factory->afterCreatingState(App\Section::class, 'stock',
    function($section, $faker){
        factory(App\Content::class, 10)->create([
            'section_id' => $section->id,
        ]);
    }
);
